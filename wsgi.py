# -*- coding: utf-8 -*-

from flask import *
import bookdb
import uuid
import string, operator
import requests


DATABASE = 'microblog.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERNAME = 'admin'
PASSWORD = 'TajneHaslo'

application = app = Flask(__name__)
app.config.from_object(__name__)
app.config['DEBUG'] = True
db = bookdb.BookDB()
#g.result = 0.0


@app.route('/')
def main_window():
    # Przekaż listę książek do szablonu "calc_list.html"
    base1 =  'http://194.29.175.241:5984/calc1/_design/utils/_view/list_active'
    #data = {'id': uuid.uuid4(), 'number1': x, 'number2': y }
    response1 = requests.get(base1)
    calculations = []
    for datas in response1.json()["rows"]:
        if datas[u'key'] not in calculations:
            calculations.append(datas[u'key'])
    return render_template('calc_list.html', calc_list=db.titles(), result='No data', calculations=calculations)
    pass


@app.route('/calc/<calc_id>/')
def calc(calc_id):
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"

    return render_template('book_detail.html',book_details=db.title_info(calc_id))
    pass

@app.route('/add_calc', methods=['POST', 'GET'])
def add_calc():
    base1 =  'http://194.29.175.241:5984/calc1/_design/utils/_view/list_active'
    #data = {'id': uuid.uuid4(), 'number1': x, 'number2': y }
    response1 = requests.get(base1)
    calculations = []
    for datas in response1.json()["rows"]:
        if datas[u'key'] not in calculations:
            calculations.append(datas[u'key'])
    try:
        result = rpn(request.form['title'])
        #print result
        #flash(result)
        res=result
        return render_template('calc_list.html', calc_list=db.titles(), result=result, calculations=calculations)
        pass
    except:
        flash(u'zepsules')
        pass
        return redirect('/')#url_for('main_window'))
    return redirect('/')


def rpn(data):
    ops = {'+': operator.add}
    #ops = {'+': operator.add, '-': operator.sub, '*': operator.mul, '/': operator.div}
    while True:
        try:
            st = []
            for tk in string.split(data):
                '''
                if tk in ops:
                    y,x = st.pop(),st.pop()
                    z = ops[tk](x,y)
                else:
                    z = float(tk)
                st.append(z)
                '''
                if tk.isdigit():
                    z = float(tk)
                elif tk in ops:
                    y,x = st.pop(),st.pop()
                    z = ops[tk](x,y)
                else:
                    y,x = st.pop(),st.pop()
                    base1 =  'http://194.29.175.241:5984/calc1/_design/utils/_view/list_active'
                    #data = {'id': uuid.uuid4(), 'number1': x, 'number2': y }
                    response1 = requests.get(base1)
                    #print response1
                    #print(response1.json()["rows"])
                    data1 = response1.json()["rows"]
                    calculations = []
                    for datas1 in response1.json()["rows"]:
                        if datas1[u'key'] not in calculations:
                            calculations.append(datas1[u'key'])
                    #print calculations
                    if unicode(tk) not in calculations:
                        flash(u'nie ma takiego numeru... no, obliczenia')
                        return 0.0
                        break
                    for datas in data1:
                        #print datas
                        if datas[u'key']==unicode(tk):
                            #print 'good obl'
                            base2 = datas[u'value'][u'host']
                            data2 = {u'id': uuid.uuid4(), u'number1': x, u'number2': y }
                            response2 = requests.get(base2, data=data2)
                            #print response2
                            #print response2.json()
                            if response2.json()['success']==True:
                                z = response2.json()['result']
                                #print z
                            else:
                                flash(u'blad obliczenia')
                            break
                    #if response.json()['success']
                    #break
                st.append(z)
            assert len(st)<=1
            if len(st)==1:
                flash(u'obliczenie succesful')
                return float(st.pop())
        except EOFError:
            break
        except:
            flash(u'inny blad obliczenia')
            #print('error')
            break
    return 0.0



if __name__ == '__main__':
    #app.run(debug=True,port=4445)

    app.run(debug=True)